#include <CGAL/Exact_predicates_exact_constructions_kernel.h>
#include <CGAL/Simple_polygon_visibility_2.h>
//#include "/home/yonatan/workspace/cgal-kug/visibility_utils.h"
//#include "/home/yonatan/workspace/cgal-kug/cheat.h"
#include <CGAL/Arrangement_2.h>
#include <CGAL/Arr_segment_traits_2.h>
#include <CGAL/Arr_naive_point_location.h>
#include <istream>
#include <vector>
typedef CGAL::Exact_predicates_exact_constructions_kernel               Kernel2;
typedef Kernel2::Point_2                                                 Point_2;
typedef Kernel2::Segment_2                                               Segment_2;
typedef CGAL::Arr_segment_traits_2<Kernel2>                              Traits_2;
typedef CGAL::Arrangement_2<Traits_2>                                   Arrangement_2;
typedef Arrangement_2::Face_handle                                      Face_handle;
typedef Arrangement_2::Edge_const_iterator                              Edge_const_iterator;
typedef Arrangement_2::Face_const_iterator                              Face_const_iterator;
typedef Arrangement_2::Ccb_halfedge_circulator                          Ccb_halfedge_circulator;
typedef CGAL::Simple_polygon_visibility_2<Arrangement_2, CGAL::Tag_true> RSPV;

Arrangement_2 compute_visibility(Point_2 &q, std::vector<Segment_2> & segments){
	Arrangement_2 env;
	CGAL::insert_non_intersecting_curves(env,segments.begin(),segments.end());

	Arrangement_2::Face_const_handle * face;
	CGAL::Arr_naive_point_location<Arrangement_2> pl(env);
	CGAL::Arr_point_location_result<Arrangement_2>::Type obj = pl.locate(q);
	// The query point locates in the interior of a face
	face = boost::get<Arrangement_2::Face_const_handle> (&obj);

	// compute non regularized visibility area
	// Define visibiliy object type that computes regularized visibility area
	Arrangement_2 regular_output;
	RSPV regular_visibility(env);
	regular_visibility.compute_visibility(q, *face, regular_output);
	return regular_output;
}
//typedef CGAL::Arrangement_on_surface_2<Traits_2,CGAL::internal::Default_planar_topology_impl<Traits_2,Arr_default_dcel<Traits_2>,CGAL::Arr_are_all_sides_oblivious_tag<CGAL::internal::Arr_complete_left_side_category<Traits_2>::Category,CGAL::internal::Arr_complete_bottom_side_category<Traits_2>::Category,CGAL::internal::Arr_complete_top_side_category<Traits_2>::Category,CGAL::internal::Arr_complete_right_side_category<Traits_2>::Category>::result>::Traits>::Face_iterator Face_iterator
void print_arrangement(Arrangement_2 & arr){
	auto asdf = arr._dcel().faces;
	asdf.
	std::cout << "Regularized visibility region is "
	            << arr.number_of_faces()
	            << " Faces:" << std::endl;
	  for (Face_const_iterator fit = arr.faces_begin(); fit != arr.faces_end(); ++fit)
	    std::cout << "\nFace: "  << std::endl;
	  	print_face(fit);
}


void print_face (Arrangement_2 :: Face_const_handle f )
{
// Print the outer boundary .
	f−>is_unbounded();
}

//void print_face(Arrangement_2::Face_const_handle * fit){
//	fit->
//	auto asdf = fit->current_iterator()->next_link->Arr_face();
//	std::cout << "Regularized visibility region is "
//		            << arr.number_of_faces()
//		            << " Faces:" << std::endl;
//		  for (Edge_const_iterator eit = fit->current_iterator()->edges_begin(); eit != arr.edges_end(); ++eit)
//		    std::cout << "[" << eit->source()->point() << " -> " << eit->target()->point() << "]" << std::endl;
//}
int main() {
  //create environment
  Point_2 p1(0,4), p2(0,0), p3(3,2), p4(4,0), p5(4,4), p6(1,2);
  std::vector<Segment_2> segments;
  segments.push_back(Segment_2(p1, p2));
  segments.push_back(Segment_2(p2, p3));
  segments.push_back(Segment_2(p3, p4));
  segments.push_back(Segment_2(p4, p5));
  segments.push_back(Segment_2(p5, p6));
  segments.push_back(Segment_2(p6, p1));

  // find the face of the query point
  // (usually you may know that by other means)
  Point_2 q(0.5, 2);
  Arrangement_2 regular_output = compute_visibility(q, segments);
  print_arrangement(regular_output);

  return 0;
}


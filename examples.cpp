#include <numeric>
#include <string>
#include <functional>

#include <iterator>
#include <CGAL/Cartesian.h>
#include <CGAL/MP_Float.h>
#include <CGAL/Quotient.h>
#include <CGAL/Arr_segment_traits_2.h>
#include <CGAL/Arrangement_2.h>
#include <CGAL/Arr_segment_traits_2.h>
#include <CGAL/Arr_overlay_2.h>
#include <CGAL/Arr_default_overlay_traits.h>
#include <CGAL/Exact_predicates_exact_constructions_kernel.h>
#include <CGAL/Simple_polygon_visibility_2.h>
//#include "/home/yonatan/workspace/cgal-kug/visibility_utils.h"
//#include "/home/yonatan/workspace/cgal-kug/cheat.h"
#include <CGAL/Arrangement_2.h>
#include <CGAL/Arr_segment_traits_2.h>
#include <CGAL/Arr_naive_point_location.h>
#include <istream>
#include <vector>
#include <assert.h>
#include <deque>
#include <boost/geometry.hpp>
#include <boost/geometry/geometries/point_xy.hpp>
#include <boost/geometry/geometries/polygon.hpp>
#include <boost/geometry/io/wkt/wkt.hpp>


#include <boost/mpl/assert.hpp>
#include <CGAL/Arr_tags.h>
#include <CGAL/Arr_enums.h>
#include <CGAL/HalfedgeDS_iterator.h>
#include <CGAL/Arrangement_2/Arrangement_2_iterators.h>
#include <CGAL/In_place_list.h>
#include <CGAL/Arr_default_dcel.h>
#include <CGAL/Arr_observer.h>
#include <CGAL/Arr_accessor.h>
#include <CGAL/Arrangement_2/Arr_traits_adaptor_2.h>
#include <CGAL/function_objects.h>
#include <CGAL/Iterator_project.h>
#include <CGAL/Iterator_transform.h>
#include <map>
#include <vector>
#include <algorithm>
#include <CGAL/Arr_tags.h>
#include <CGAL/Arrangement_on_surface_2.h>
#include <CGAL/Arrangement_2/Arr_default_planar_topology.h>


typedef CGAL::Quotient<CGAL::MP_Float> Number_type;
typedef CGAL::Cartesian<Number_type> Kernel;
typedef CGAL::Arr_segment_traits_2<Kernel> Traits_2;
typedef Traits_2::Point_2 Point_2;
typedef Traits_2::X_monotone_curve_2 Segment_2;
typedef CGAL::Arrangement_2<Traits_2> Arrangement_2;
typedef Arrangement_2::Ccb_halfedge_const_circulator Ccb_halfedge_const_circulator;
typedef Arrangement_2::Face_const_iterator Face_const_iterator;
typedef Arrangement_2::Vertex_const_iterator Vertex_const_iterator;
typedef Arrangement_2::Edge_const_iterator Edge_const_iterator;
typedef CGAL::Arr_default_overlay_traits<Arrangement_2> Overlay_traits;
typedef CGAL::Simple_polygon_visibility_2<Arrangement_2, CGAL::Tag_true> RSPV;
typedef Arrangement_2::Vertex_const_handle Vertex_const_handle;
typedef CGAL::Quotient<CGAL::MP_Float> Number_type;
typedef CGAL::Cartesian<Number_type> Kernel;
typedef CGAL::Arr_segment_traits_2<Kernel> Traits_2;
typedef Traits_2::Point_2 Point_2;
typedef Traits_2::X_monotone_curve_2 Segment_2;
typedef CGAL::Arrangement_2<Traits_2> Arrangement_2;
typedef Arrangement_2::Ccb_halfedge_const_circulator Ccb_halfedge_const_circulator;
typedef Arrangement_2::Face_const_iterator Face_const_iterator;
typedef Arrangement_2::Vertex_const_iterator Vertex_const_iterator;
typedef Arrangement_2::Edge_const_iterator Edge_const_iterator;
typedef boost::geometry::model::polygon<boost::geometry::model::d2::point_xy<double> > polygon;
typedef boost::geometry::model::d2::point_xy<double> point_xy;


void print_neighboring_vertices(Arrangement_2::Vertex_const_handle v) {
	if (v->is_isolated()) {
		std::cout << "The vertex (" << v->point() << ") is isolated"
				<< std::endl;
		return;
	}
	Arrangement_2::Halfedge_around_vertex_const_circulator first, curr;
	first = curr = v->incident_halfedges();
	std::cout << "The neighbors of the vertex (" << v->point() << ") are:";
	do {
		// Note that the current halfedge is directed from u to v:
		Arrangement_2::Vertex_const_handle u = curr->source();
		std::cout << " (" << u->point() << ")";
	} while (++curr != first);
	std::cout << std::endl;
}

void print_ccb(Arrangement_2::Ccb_halfedge_const_circulator circ) {
	Ccb_halfedge_const_circulator curr = circ;
	std::cout << "(" << curr->source()->point() << ")";
	do {
		std::cout << " [" << curr->curve() << "] " << "("
				<< curr->target()->point() << ")";
	} while (++curr != circ);
	std::cout << std::endl;
}

void print_face(Arrangement_2::Face_const_handle f) {
	// Print the outer boundary.
	if (f->is_unbounded())
		std::cout << "Unbounded face. " << std::endl;
	else {
		std::cout << "Outer boundary: ";
		print_ccb(f->outer_ccb());
	}
	// Print the boundary of each of the holes.
	Arrangement_2::Hole_const_iterator hi;
	int index = 1;
	for (hi = f->holes_begin(); hi != f->holes_end(); ++hi, ++index) {
		std::cout << " Hole #" << index << ": ";
		print_ccb(*hi);
	}
	// Print the isolated vertices.
	Arrangement_2::Isolated_vertex_const_iterator iv;
	for (iv = f->isolated_vertices_begin(), index = 1;
			iv != f->isolated_vertices_end(); ++iv, ++index) {
		std::cout << " Isolated vertex #" << index << ": " << "(" << iv->point()
				<< ")" << std::endl;
	}
}

void print_arrangement(const Arrangement_2& arr) {
	// Print the arrangement vertices.
	Vertex_const_iterator vit;
	std::cout << arr.number_of_vertices() << " vertices:" << std::endl;
	for (vit = arr.vertices_begin(); vit != arr.vertices_end(); ++vit) {
		std::cout << "(" << vit->point() << ")";
		if (vit->is_isolated())
			std::cout << " - Isolated." << std::endl;
		else
			std::cout << " - degree " << vit->degree() << std::endl;
	}
	// Print the arrangement edges.
	Edge_const_iterator eit;
	std::cout << arr.number_of_edges() << " edges:" << std::endl;
	for (eit = arr.edges_begin(); eit != arr.edges_end(); ++eit)
		std::cout << "[" << eit->curve() << "]" << std::endl;
	// Print the arrangement faces.
	Face_const_iterator fit;
	std::cout << arr.number_of_faces() << " faces:" << std::endl;
	for (fit = arr.faces_begin(); fit != arr.faces_end(); ++fit)
		print_face(fit);
}

/////was points:
typedef CGAL::Simple_polygon_visibility_2<Arrangement_2, CGAL::Tag_true> RSPV;

Arrangement_2 create_arrangement(std::vector<Segment_2> & segments){
	Arrangement_2 arr;
	CGAL::insert (arr, &segments[0], &segments[segments.size()-1]);
	return arr;
}

Arrangement_2 segments_to_arr(std::vector<Segment_2> & segments){
	Arrangement_2 env;
	CGAL::insert_non_intersecting_curves(env, segments.begin(), segments.end());
	return env;
}

Arrangement_2 compute_visibility(Point_2 &q, Arrangement_2& input_polygon) {

	Arrangement_2::Face_const_handle * face;
	CGAL::Arr_naive_point_location<Arrangement_2> pl(input_polygon);
	CGAL::Arr_point_location_result<Arrangement_2>::Type obj = pl.locate(q);
	// The query point locates in the interior of a face
	face = boost::get<Arrangement_2::Face_const_handle>(&obj);

	// compute non regularized visibility area
	// Define visibiliy object type that computes regularized visibility area
	Arrangement_2 regular_output;
	RSPV regular_visibility(input_polygon);
	regular_visibility.compute_visibility(q, *face, regular_output);
	return regular_output;
}


Arrangement_2 tmp(std::vector<Segment_2> & segments){
	Arrangement_2 env;
	CGAL::insert_non_intersecting_curves(env, segments.begin(), segments.end());
	return env;
//	Arrangement_2::Face_const_handle * face;
//	CGAL::Arr_naive_point_location<Arrangement_2> pl(env);
//	CGAL::Arr_point_location_result<Arrangement_2>::Type obj = pl.locate(q);
//	// The query point locates in the interior of a face
//	return boost::get<Arrangement_2::Face_const_handle>(&obj);
}

polygon cast_to_polygon(Arrangement_2 & arr){
	Face_const_iterator begin = arr.faces_begin();
	Face_const_iterator end = arr.faces_end() ;
	Face_const_iterator unbounded_face;

	assert(std::distance(begin, end) == 2);
	if (begin->is_unbounded()){
		unbounded_face = begin;
	}
	else if(end->is_unbounded()){
		unbounded_face = end;
	}
	else{
		std::cout << "no unbounded face\n";
		assert(0);
	}

	for(begin= arr.faces_begin(); begin != end; begin++){
		if(begin->is_unbounded()){
			assert(not unbounded_face);
			unbounded_face = begin;
		}
	}

	Arrangement_2::Hole_const_iterator face = unbounded_face->holes_begin();


	Ccb_halfedge_const_circulator circ = *face;
	Ccb_halfedge_const_circulator curr = circ;

	std::ostringstream s;
	s << "POLYGON((";
	do {
		Vertex_const_handle source =  curr->source();

		s << CGAL::to_double(curr->source()->point().x()) << " " << CGAL::to_double(curr->source()->point().y()) << ", ";
	} while (++curr != circ);
	s << CGAL::to_double(curr->source()->point().x()) << " " << CGAL::to_double(curr->source()->point().y()) ;
	s << "))";
	std::cout << s.str() << std::endl;
	polygon po;

	boost::geometry::read_wkt(s.str(), po);
	return po;
}

int lambda__(int i, polygon p){
	return i + boost::geometry::area(p);
}

int main() {

	Point_2 p1(0, 4), p2(0, 0), p3(3, 2), p4(4, 0), p5(4, 4), p6(1, 2);
	std::vector<Segment_2> segments;
	segments.push_back(Segment_2(p1, p2));
	segments.push_back(Segment_2(p2, p3));
	segments.push_back(Segment_2(p3, p4));
	segments.push_back(Segment_2(p4, p5));
	segments.push_back(Segment_2(p5, p6));
	segments.push_back(Segment_2(p6, p1));
	Arrangement_2 input_polygon_as_arrangement = segments_to_arr(segments);
	std::vector<Point_2> cameras;
	Point_2 q1(0.5, 2);
	Point_2 q2(3.5, 1.5);
	Point_2 q3(3.75, 3);
	cameras.push_back(q1);
	cameras.push_back(q2);
	cameras.push_back(q3);

	Arrangement_2 overlay_results[cameras.size()+1];

	for (int i = 0; i < cameras.size(); ++i){
		std::cout << i << std::endl;
		Arrangement_2 curr_vis = compute_visibility(cameras[i], input_polygon_as_arrangement);

		std::cout << "begining overlay" << std::endl;
		Overlay_traits * traits = new Overlay_traits();
		CGAL::overlay(curr_vis, overlay_results[i], overlay_results[i+1], *traits);

	}


	std::cout << "after overlay" << std::endl;
	Arrangement_2 overlay_result = overlay_results[cameras.size()];
	std::cout << "pre cast overlay" << std::endl;
//	Arrangement_2 q1_vis = compute_visibility(q1, input_polygon_as_arrangement);
//	Arrangement_2 q2_vis = compute_visibility(q2, input_polygon_as_arrangement);



	const polygon overlay_polygon = cast_to_polygon(overlay_result);
	const polygon input_polygon = cast_to_polygon(input_polygon_as_arrangement);
	std::list<polygon> difference;
	boost::geometry::difference(input_polygon, overlay_polygon, difference);

//	std::cout << "input polygon area\t" << boost::geometry::area(input_polygon) ;
	std::cout << "size\t" << difference.size() << std::endl;
	polygon tmp = *difference.begin();
//	std::vector<boost::geometry::model::d2::point_xy<double> >
	std::cout << "area: " << std::accumulate(difference.begin(), difference.end(), 0, lambda__);

	std::vector<point_xy>::iterator outer_it = tmp.outer().begin();
	std::vector<point_xy>::iterator outer_end = tmp.outer().end();
	for (; outer_it!=  outer_end; outer_it++ ){
		std::cout << "outerX " << outer_it->x() << std::endl;
		std::cout << "outerY" << outer_it->y() << std::endl;
	}


	for (int i =0; i< tmp.inners().size(); i++){
		std::vector<point_xy>::iterator inner_it = tmp.inners()[i].begin();
		std::vector<point_xy>::iterator inner_end = tmp.inners()[i].end();
		for (; inner_it!=  inner_end; inner_it++){
			std::cout << inner_it->x() << std::endl;
			std::cout << inner_it->y() << std::endl;
		}
	}
//	for (std::list<polygon >::iterator i = difference.begin(); i != difference.end(); i++){
//		for(auto it1 = boost::begin(bg::exterior_ring(pol)); it1 != boost::end(bg::exterior_ring(pol)); ++it1){
//
//			std::cout << *i;
//		}
//	for (std::vector<boost::geometry::point_xy<double,cartesian> >::iterator j = i->inners().begin()->begin(); j != i->inners().begin()->end(); j++){
//	}
	print_arrangement(overlay_result);

	std::cout << "code ended succ" << std::endl;
	return (0);
}

import sys
import re
def replace(matchobj):
    tmp = matchobj.group()
    return str(eval(tmp))[:4]

rr = r"([\d+e.-]+/[\d+e.-]+)"

for line in sys.stdin:
    print re.sub(rr, replace, line)

